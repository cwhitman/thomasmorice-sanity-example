import React, { useEffect, useContext, useRef } from "react";
import { myContext } from "../provider";
import PropTypes from "prop-types";
import useOnScreen from "../hooks/useOnScreen";

const SectionContainer = ({
  children,
  sectionKey,
  anchorInMenu,
  anchorTitle,
  onScreen,
}) => {
  const sectionRef = useRef();
  const refOnScreen = useOnScreen(sectionRef);
  const context = useContext(myContext);

  useEffect(() => {
    context.addPageSection({
      sectionKey: sectionKey,
      anchorInMenu,
      anchorTitle,
      ref: sectionRef,
    });
  }, [sectionRef]);

  useEffect(() => {
    onScreen(refOnScreen);
    if (anchorInMenu) {
      context.set_currentSectionKey(sectionKey);
    }
  }, [refOnScreen]);
  return <div ref={sectionRef}>{children}</div>;
};

SectionContainer.defaultProps = {
  sectionKey: null,
  anchorInMenu: false,
  anchorTitle: "",
  ref: null,
  onScreen: null,
};

SectionContainer.propTypes = {
  sectionKey: PropTypes.number.isRequired,
  anchorInMenu: PropTypes.bool,
  anchorTitle: PropTypes.string,
  ref: PropTypes.object,
  onScreen: PropTypes.func,
};

export default SectionContainer;
