import React from "react";
import styled, { css } from "styled-components";
import Image from "gatsby-image";
import { Container, Paragraph } from "../components";
import { size } from "../theme/theme";

const Title = styled.div`
  font: normal normal 900 18px/29px "Montserrat", serif;
  letter-spacing: 0.05em;
  margin-bottom: 80px;
  max-width: 480px;
  @media ${size.s} {
    font-size: 23px;
    line-height: 30px;
  }
  @media ${size.m} {
    font-size: 25px;
    line-height: 33px;
  }
  @media ${size.l} {
    font-size: 27px;
    line-height: 40px;
    max-width: 530px;
  }
`;

const Header = styled.div`
  position: relative;
  color: #f7f7f7;
  height: 230px;
  background-color: ${(props) => props.backgroundColor};
  ${(props) =>
    props.backgroundPattern &&
    css`
      background-image: url(${props.backgroundPattern});
    `}
`;

const LogoAndCompany = styled.div`
  display: flex;
  align-items: center;
  height: 130px;

  @media ${size.m} {
    height: 100%;
  }
`;

const Logo = styled.div`
  position: absolute;
  margin-left: 12px;
  z-index: 9;

  svg {
    height: auto;
    width: 42px;
  }
  @media ${size.m} {
    margin-left: 6px;
  }
  @media ${size.l} {
    margin-left: 42px;
  }
`;

const CompanyAndTechnologies = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 60px;
  width: 100%;
  margin-left: 32px;
  @media ${size.m} {
    margin-left: 0;
  }
`;

const CompanyName = styled.div`
  font: normal normal 900 24px/28px "Montserrat", serif;
  @media ${size.m} {
    font-size: 28px;
    line-height: 33px;
  }
  ${(props) =>
    props.darkText &&
    css`
      color: #2b2424;
    `}
`;

const Technologies = styled.div`
  display: flex;
  align-items: center;
  font: normal normal 600 12px/23px "Montserrat", serif;
  letter-spacing: -0.03em;
  color: white;

  ${(props) =>
    props.darkText &&
    css`
      color: #2b2424;
    `}

  &:before {
    margin-right: 12px;
    display: block;
    content: "";
    width: 38px;
    height: 1px;
    background-color: #f7f7f7;
    ${(props) =>
      props.darkText &&
      css`
        background-color: #2b2424;
      `}
  }
`;

const ParagraphContainer = styled.div`
  margin-top: 50px;
`;

const Illustration = styled(Image)`
  width: ${(props) => props.width.default};
  height: auto;
  margin: 0 auto;
  margin-top: -98px;
  @media ${size.s} {
    width: ${(props) => props.width.s};
  }
  @media ${size.m} {
    position: absolute !important;
    width: ${(props) => props.width.m};
    right: 30px;
    bottom: ${(props) => props.bottom.m};
  }
  @media ${size.xl} {
    width: ${(props) => props.width.xl};
    bottom: ${(props) => props.bottom.xl};
    left: 60%;
    right: 0;
  }
`;

const WorkExperience = ({
  title,
  text,
  bannerColor,
  bannerPattern,
  companyName,
  technologies,
  logo,
  illustration,
  illustrationSizes,
  darkText,
}) => {
  return (
    <>
      <Container>
        <Title> {title}</Title>
      </Container>
      <div style={{ position: "relative" }}>
        <Header backgroundColor={bannerColor} backgroundPattern={bannerPattern}>
          <LogoAndCompany>
            <Logo> {logo} </Logo>
            <Container>
              <CompanyAndTechnologies>
                <CompanyName darkText={darkText}>{companyName}</CompanyName>
                <Technologies darkText={darkText}>
                  {technologies.join(" · ")}
                  {/* {technologies.map((technology) => technology)} */}
                </Technologies>
              </CompanyAndTechnologies>
            </Container>
          </LogoAndCompany>
        </Header>
        <Illustration
          width={illustrationSizes.width}
          bottom={illustrationSizes.bottomPosition}
          fluid={illustration}
        ></Illustration>
        <ParagraphContainer>
          <Container>
            <Paragraph smallVersion>{text}</Paragraph>
          </Container>
        </ParagraphContainer>
      </div>
    </>
  );
};

export default WorkExperience;
