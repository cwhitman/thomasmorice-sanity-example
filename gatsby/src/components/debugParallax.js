import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Container = styled.div``;

const DebugParallax = ({}) => (
  <Container>
    <div
      style={{
        position: "absolute",
        backgroundColor: "red",
        top: sectionJoinUniversity.current.getBoundingClientRect().top,
        height: "1px",
        width: "100%",
      }}
    ></div>
  </Container>
);

DebugParallax.defaultProps = {};

DebugParallax.propTypes = {};

export default DebugParallax;
