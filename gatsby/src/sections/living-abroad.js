import React from "react";
import { Container, Paragraph, RotatingButton } from "../components";
import styled, { css } from "styled-components";
import TripSvg from "../images/trip.inline.svg";
import Link from "gatsby-link";
import { size } from "../theme/theme";
import { navigate } from "gatsby";

const Title = styled.div`
  font: normal normal 700 28px/45px "Playfair Display", serif;
  letter-spacing: 0.05em;
  margin-bottom: 45px;
  @media ${size.xs} {
    font-size: 33px;
    line-height: 55px;
  }
  @media ${size.m} {
    max-width: 600px;
    font-size: 49px;
    line-height: 70px;
    margin-top: 50px;
  }
`;

const BackgroundImage = styled.div`
  position: relative;
  height: auto;
  width: 100%;
  margin-left: 10px;
  margin-top: -50%;
  svg {
    width: 100%;
    height: auto;
    @media ${size.m} {
      width: 80%;
      margin-left: 15%;
    }
    @media ${size.l} {
      margin-top: 10%;
      width: 55%;
      margin-left: 20%;
    }
  }
`;

const livingAbroad = ({ ressources }) => {
  return (
    <div style={{ overflow: "hidden" }}>
      <Container>
        <Title>I finally landed my first job as a web developer</Title>
        <Paragraph>
          Having tasted the satisfaction of an accomplishment, And after two
          amazing years at Optimind Winter, I was now looking for my new
          challenge. Living abroad was something I always wanted to do.
        </Paragraph>
        <div style={{ marginTop: "50px" }}>
          <Paragraph>
            My girlfriend and I chose Sweden as our new home. It is easy to
            adapt since almost everyone speaks English there. Stockholm is such
            a nice city, with a very good environment quality level, many
            professional opportunities, and so much more to offer.
          </Paragraph>
        </div>
      </Container>
      <BackgroundImage>
        <TripSvg />
      </BackgroundImage>
      <Container style={{ marginTop: "50px" }}>
        <RotatingButton
          style={{ maxWidth: "450px" }}
          frontText={"What happened in Sweden"}
          backText={"Check out my experiences"}
          onClick={() => navigate("/experiences/")}
        />
      </Container>
    </div>
  );
};

export default livingAbroad;
