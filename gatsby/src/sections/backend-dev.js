import React from "react";
import { Container, TitleAndAnnotation } from "../components";
import Image from "gatsby-image";
import styled from "styled-components";
import { size } from "../theme/theme";
import Plx from "react-plx";

const ImageContainer = styled(Plx)`
  width: 100%;
  margin-left: 20%;
  margin-top: 45px;
  margin-bottom: -180px;
  @media ${size.m} {
    margin-top: 0;
    margin-left: 60%;
    max-width: 500px;
  }
`;

const BackendDev = ({ ressources }) => {
  // const gatsbyImage = useRef(null);
  // const imgOnScreen = useOnScreen(gatsbyImage);
  return (
    <Container>
      <TitleAndAnnotation
        title={
          <h2>
            I started working as a backend developer full-time at 26 years old,
            for banks and old systems.
          </h2>
        }
        annotation={
          <div className="bigger-annotation">
            But I quickly realize I was missing something, that part of
            creativity into my daily work. I wanted to play with colors &
            typography, with spaces & places.
          </div>
        }
      />
      <ImageContainer
        parallaxData={[
          {
            start: "self",
            startOffset: "-20vh",
            end: "self",
            endOffset: "150vh",
            properties: [
              {
                startValue: 0,
                endValue: -120,
                property: "translateY",
              },
              {
                startValue: -150,
                endValue: 30,
                property: "translateX",
              },
            ],
          },
        ]}
      >
        <Image fluid={ressources.smokeImageFluid} />
      </ImageContainer>
    </Container>
  );
};

export default BackendDev;
