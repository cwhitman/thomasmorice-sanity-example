import React from "react";
import { Container, Paragraph } from "../components";
import Image from "gatsby-image";
import styled from "styled-components";
import { size } from "../theme/theme";

const FullScreenContainer = styled.div`
  position: relative;
  padding-bottom: 160px;
  @media ${size.m} {
    padding-bottom: 300px;
  }
  @media ${size.l} {
    padding-bottom: 390px;
  }
`;

const ImageContainer = styled.div`
  position: absolute;
  opacity: 0.8;
  z-index: 1;
  height: 100%;
  width: 100%;
  top: 0;
`;

const GradientOnImage = styled.div`
  position: absolute;
  z-index: 2;
  height: 100%;
  width: 100%;
  background: linear-gradient(
    180deg,
    rgba(255, 255, 255, 1) 0%,
    rgba(255, 255, 255, 0.85) 38%,
    rgba(255, 255, 255, 0.4) 70%,
    rgba(255, 255, 255, 0.85) 93%,
    rgba(255, 255, 255, 1) 100%
  );
`;

const trustMeWebDev = ({ ressources }) => {
  return (
    <FullScreenContainer>
      <Container>
        <Paragraph>
          Not many companies were brave enough to take me as a web developer, so
          I decided to be a self-entrepreneur, and I started making websites for
          small companies.
        </Paragraph>
      </Container>
      <Container style={{ marginTop: "50px" }}>
        <Paragraph>
          From a static site for a beauty salon to an online shop for a butcher,
          I managed to get some project done within a couple of years, and I was
          finally able to sell myself as a web developer.
        </Paragraph>
      </Container>
      <ImageContainer>
        <Image
          style={{
            position: "absolute",
            height: "100%",
            width: "100%",
            bottom: 0,
          }}
          imgStyle={{
            backgroundPosition: "center bottom",
            objectPosition: "center bottom",
          }}
          fluid={ressources.selfEntrepreneurImageFluid}
        />
        <GradientOnImage />
      </ImageContainer>
    </FullScreenContainer>
  );
};

export default trustMeWebDev;
