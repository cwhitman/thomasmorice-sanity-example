import React, { useRef, useLayoutEffect } from "react";
import { Container, Paragraph } from "../components";
import Image from "gatsby-image";
import styled, { css } from "styled-components";
import { size } from "../theme/theme";
import useOnScreen from "../hooks/useOnScreen";

const Space = styled.div`
  margin: 80px 0;
  @media ${size.m} {
    margin: 110px 0;
  }
`;

const GatsbyImage = styled(Image)`
  transition: transform 0s;
  width: 80%;
  top: 30px;
  right: -30px;
  opacity: 0.2;
  @media ${size.s} {
    width: 60%;
  }
  @media ${size.m} {
    top: 0;
    right: -80px;
    max-width: 500px;
  }
  ${(props) =>
    props.onScreen &&
    css`
      transition: transform 40s ease;
      transform: translate(-100px, -30px);
    `}
`;

const LetsBeHonest = ({ paraOne, paraTwo, image }) => {
  const gatsbyImage = useRef(null);
  const imgOnScreen = useOnScreen(gatsbyImage);
  useLayoutEffect(() => {}, []);

  return (
    <>
      <Container>
        <Paragraph>
          {paraOne}
        </Paragraph>
        <Space />
        <div ref={gatsbyImage}>
          <GatsbyImage
            style={{ position: "absolute" }}
            fluid={image}
            onScreen={imgOnScreen}
          />
        </div>
        <Paragraph>
          {paraTwo}
        </Paragraph>
      </Container>
    </>
  );
};

export default LetsBeHonest;
