export const getChildImageSharpFromFilename = ({
  nodes,
  filename,
  imgType = "fluid",
}) => {
  const childImageSharp = nodes.find((o) => {
    if (o.childImageSharp) {
      return o.childImageSharp[imgType].originalName.includes(filename);
    }
    return null;
  }).childImageSharp[imgType];
  return childImageSharp;
};
