import React, { useEffect, useContext, useRef, useCallback } from "react";
import { getChildImageSharpFromFilename } from "../utils";
import {
  Container,
  TextReveal,
  ScrollMore,
  Headline,
  WorkExperience,
  Layout,
  SEO,
} from "../components";
import { myContext } from "../provider";
import useOnScreen from "../hooks/useOnScreen";
import styled, { css } from "styled-components";
import { size } from "../theme/theme";
import BannerflowLogo from "../images/experiences/bannerflow.inline.svg";
import NaturalCyclesLogo from "../images/experiences/natural-cycles.inline.svg";
import QredLogo from "../images/experiences/qred.inline.svg";
// import ApegroupLogo from "../images/experiences/apegroup.inline.svg";
import ApegroupLogo from "../images/experiences/apegroup-logo.png";
import ApegroupPattern from "../images/experiences/apegroup-pattern.png";

const Section = styled.div`
  z-index: 99;
  ${(props) => css`
    ${
      props.underHeader
        ? `
            height: calc(100vh - ${props.theme.spaces.headerHeight});
            margin: 10vh 0 -40vh 0;
            display: flex;
          `
        : css`
            margin: 140px 0 50px;
            @media ${size.m} {
              margin: 230px 0 80px;
            }
          `
    }
    ${
      props.snapScroll &&
      `
        scroll-snap-align: start;
      `
    }
    ${props.style}
  `};
`;

const ExperiencesPage = ({ data }) => {
  const context = useContext(myContext);

  const sectionBannerflow = useRef(null);
  const sectionNaturalCycles = useRef(null);
  const sectionQred = useRef(null);
  const sectionApegroup = useRef(null);

  const sectionBannerflowOnScreen = useOnScreen(sectionBannerflow);
  const sectionNaturalCyclesOnScreen = useOnScreen(sectionNaturalCycles);
  const sectionQredOnScreen = useOnScreen(sectionQred);
  const sectionApegroupOnScreen = useOnScreen(sectionApegroup);

  const onLoad = () => {
    const sections = [
      {
        key: 1,
        title: "Landing in Sweden",
        ref: sectionBannerflow,
      },
      {
        key: 2,
        title: "Trying myself as a manager",
        ref: sectionNaturalCycles,
      },
      {
        key: 3,
        title: "Back to the basics",
        ref: sectionQred,
      },
      {
        key: 4,
        title: "My current home",
        ref: sectionApegroup,
      },
    ];

    context.set_pageSections(sections);
  };

  useEffect(onLoad, []);

  const setCurrentSectionKey = useCallback(() => {
    if (sectionBannerflowOnScreen) {
      context.set_currentSectionKey(1);
    } else if (sectionNaturalCyclesOnScreen) {
      context.set_currentSectionKey(2);
    } else if (sectionQredOnScreen) {
      context.set_currentSectionKey(3);
    } else if (sectionApegroupOnScreen) {
      context.set_currentSectionKey(4);
    }
  });

  useEffect(() => {
    setCurrentSectionKey();
  }, [
    sectionBannerflowOnScreen,
    sectionNaturalCycles,
    sectionQredOnScreen,
    sectionApegroupOnScreen,
    setCurrentSectionKey,
  ]);

  return (
    <Layout>
      <SEO
        title="Thomas Morice: My experiences in Sweden"
        description="After few different jobs as a backend developer in France, I moved to Sweden. Discover some of my experiences as a web developer since I joined this beautiful country. "
      />
      <Section ref={sectionBannerflow} underHeader>
        <Container>
          <div>
            <Headline
              leftElem={<div>Things that made me who I am </div>}
              rightElem={<div>today</div>}
            ></Headline>
          </div>
          <div>
            <h1>
              <TextReveal rotate>Dive into some of</TextReveal>
              <TextReveal rotate delay={0.11}>
                my experiences and learnings
              </TextReveal>
            </h1>
          </div>
          <div>
            <ScrollMore />
          </div>
        </Container>
      </Section>
      <Section>
        <WorkExperience
          title={
            "My first experience in Stockholm was a lot of learnings and so much fun."
          }
          text={
            "I’ve learned many things around Wordpress, php and css. Together with the designer & the marketing team, we maintained the main website, and we built the blog from scratch. Lots of learnings & lots of fun! I kept friends and good memories from that experience"
          }
          companyName={"Bannerflow"}
          bannerColor={"#010006"}
          technologies={["PHP", "Laravel", "Gulp"]}
          logo={<BannerflowLogo />}
          illustration={getChildImageSharpFromFilename({
            nodes: data.allFile.nodes,
            filename: "bannerflow",
          })}
          illustrationSizes={{
            width: {
              default: "300px",
              s: "390px",
              m: "450px",
              xl: "550px",
            },
            bottomPosition: {
              m: "78px",
              xl: "48px",
            },
          }}
        ></WorkExperience>
      </Section>

      <Section ref={sectionNaturalCycles}>
        <WorkExperience
          title={
            "I then took an opportunity to try myself as a Lead Web Developer at Natural cycles"
          }
          text={
            "Natural Cycles has been an inspiring experience. Being a manager is not something we can easily improvise. I put a lot of heart, but somehow I felt like it wasn’t the right place at the right time. I’ve learned many things, technically but even more about my inner self. And they have an amazing product!"
          }
          companyName={"Natural cycles"}
          bannerColor={"#68175B"}
          technologies={["Angular", "Wordpress", "Webpack", "Netlify CMS"]}
          logo={<NaturalCyclesLogo />}
          illustration={getChildImageSharpFromFilename({
            nodes: data.allFile.nodes,
            filename: "natural-cycles",
          })}
          illustrationSizes={{
            width: {
              default: "190px",
              s: "230px",
              m: "270px",
              xl: "330px",
            },
            bottomPosition: {
              m: "105px",
              xl: "70px",
            },
          }}
        ></WorkExperience>
      </Section>

      <Section ref={sectionQred}>
        <WorkExperience
          title={
            "For my next experience, I wanted to go back to the basics, and learn more about frontend"
          }
          text={
            "Qred was like a family, I worked there for almost two years, implementing the main website, maintaining it by adding new features and languages. I learned a lot more about frontend in general, specifically Vue."
          }
          companyName={"Qred AB"}
          bannerColor={"#237B4B"}
          technologies={["Vue", "Nuxt", "React", "Styled components"]}
          logo={<QredLogo />}
          illustration={getChildImageSharpFromFilename({
            nodes: data.allFile.nodes,
            filename: "qred",
          })}
          illustrationSizes={{
            width: {
              default: "260px",
              s: "290px",
              m: "340px",
              xl: "390px",
            },
            bottomPosition: {
              m: "125px",
              xl: "100px",
            },
          }}
        ></WorkExperience>
      </Section>

      <Section ref={sectionApegroup}>
        <WorkExperience
          title={
            "Looking for a new challenge, with various project, I joined a digital agency named Apegroup"
          }
          text={
            "Apegroup is where I’m currently working, and I’ve already contributed to more than four different projects there. It is an amazing place, with a great atmosphere, where you have space & time to learn & improve. I’m thrilled to be part of this journey"
          }
          companyName={"Apegroup AB"}
          bannerPattern={ApegroupPattern}
          technologies={["React", "Next", "NodeJS", "Gatsby"]}
          logo={<img alt="Thomas Morice at Apegroup" src={ApegroupLogo} />}
          illustration={getChildImageSharpFromFilename({
            nodes: data.allFile.nodes,
            filename: "apegroup-experience",
          })}
          illustrationSizes={{
            width: {
              default: "260px",
              s: "370px",
              m: "390px",
              xl: "520px",
            },
            bottomPosition: {
              m: "233px",
              xl: "192px",
            },
          }}
          darkText
        ></WorkExperience>
      </Section>
    </Layout>
  );
};

export const query = graphql`
  query allExperiences {
    allFile(
      filter: {
        extension: { regex: "/(jpg)|(jpeg)|(png)/" }
        relativeDirectory: { eq: "experiences" }
      }
    ) {
      nodes {
        childImageSharp {
          fluid(maxWidth: 2000, quality: 70) {
            ...GatsbyImageSharpFluid_withWebp
            originalName
          }
        }
      }
    }
  }
`;

export default ExperiencesPage;
