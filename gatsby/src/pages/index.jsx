import React, { useContext, useRef, useEffect, useCallback } from "react";
import { getChildImageSharpFromFilename } from "../utils";
import {
  Hello,
  LetsBeHonest,
  BackToUniversity,
  DuringUniversity,
  BackendDev,
  TrustMeWebDev,
  LivingAbroad,
} from "../sections";
import { myContext } from "../provider";
import styled, { css } from "styled-components";
import useOnScreen from "../hooks/useOnScreen";
import { size } from "../theme/theme";
import { Layout, SEO } from "../components";
import { graphql } from 'gatsby'

const Section = styled.div`
  ${(props) => css`
    ${
      props.underHeader
        ? `
            height: calc(100vh - ${props.theme.spaces.headerHeight});
            margin: 10vh 0 -40vh 0;
            display: flex;
          `
        : css`
            margin: 140px 0 50px;
            @media ${size.m} {
              margin: 180px 0 80px;
            }
          `
    }
    ${
      props.snapScroll &&
      `
        scroll-snap-align: start;
      `
    }
    ${props.style}
  `}
`;

const IndexPage = ({ data }) => {
  const context = useContext(myContext);
  const sectionHello = useRef(null); // Before Computer science
  const sectionLetsBeHonest = useRef(null); // Before Computer science
  const sectionBackToUniversity = useRef(null); // Find the courage to reset
  const sectionDuringUniversity = useRef(null); // Find the courage to reset
  const sectionBackendDev = useRef(null); // Shift towards my passion
  const sectionTrustMeWebDev = useRef(null); // Shift towards my passion
  const sectionLivingAbroad = useRef(null); // Leap into the void

  const sectionHelloOnScreen = useOnScreen(sectionHello);
  const sectionBackToUniversityOnScreen = useOnScreen(sectionBackToUniversity);
  const sectionBackendDevOnScreen = useOnScreen(sectionBackendDev);
  const sectionLivingAbroadOnScreen = useOnScreen(sectionLivingAbroad);

  const setCurrentSectionKey = useCallback(() => {
    if (sectionHelloOnScreen) {
      context.set_currentSectionKey(1);
    } else if (sectionBackToUniversityOnScreen) {
      context.set_currentSectionKey(2);
    } else if (sectionBackendDevOnScreen) {
      context.set_currentSectionKey(3);
    } else if (sectionLivingAbroadOnScreen) {
      context.set_currentSectionKey(4);
    }
  });

  useEffect(() => {
    setCurrentSectionKey();
  }, [
    sectionHelloOnScreen,
    sectionBackToUniversityOnScreen,
    sectionBackendDevOnScreen,
    sectionLivingAbroadOnScreen,
    setCurrentSectionKey,
  ]);

  const onLoad = () => {
    const sections = [
      {
        key: 1,
        title: "Before computer science",
        ref: sectionHello,
      },
      {
        key: 2,
        title: "Find the courage to reset",
        ref: sectionBackToUniversity,
      },
      {
        key: 3,
        title: "Shift towards my passion",
        ref: sectionBackendDev,
      },
      {
        key: 4,
        title: "Leap into the void",
        ref: sectionLivingAbroad,
      },
    ];

    context.set_pageSections(sections);
  };

  useEffect(onLoad, []);

  return (
    <Layout>
      <SEO
        title="Thomas Morice: A passionate web developer"
        description="Hello, I'm Thomas, A passionate web developer originally from France and currently living in Sweden. Discover more about me and my passion"
      />
      <Section underHeader ref={sectionHello}>
        <Hello
          elemLeft={data.sanityHome.hello.elemLeft}
          elemRight={data.sanityHome.hello.elemRight}
          heroText={data.sanityHome.hello.heroText}
        />
      </Section>
      <Section snapScroll ref={sectionLetsBeHonest}>
        <LetsBeHonest
          paraOne={data.sanityHome.letsBeHonest.paraOne}
          paraTwo={data.sanityHome.letsBeHonest.paraTwo}
          image={data.sanityHome.letsBeHonest.image.asset.fluid}
        />
      </Section>
      <Section snapScroll ref={sectionBackToUniversity}>
        <BackToUniversity
          mainContent={data.sanityHome.backToUni.main}
          accent={data.sanityHome.backToUni.accent}
        />
      </Section>

      <Section ref={sectionDuringUniversity}>
        <DuringUniversity 
          content={data.sanityHome.duringUni}
        />
      </Section>

      <Section ref={sectionBackendDev}>
        <BackendDev
          ressources={{
            smokeImageFluid: getChildImageSharpFromFilename({
              nodes: data.allFile.nodes,
              filename: "smoke",
            }),
          }}
        />
      </Section>
      <Section ref={sectionTrustMeWebDev}>
        <TrustMeWebDev
          ressources={{
            selfEntrepreneurImageFluid: getChildImageSharpFromFilename({
              nodes: data.allFile.nodes,
              filename: "self-entrepreneur",
            }),
          }}
        />
      </Section>
      <Section ref={sectionLivingAbroad}>
        <LivingAbroad />
      </Section>
    </Layout>
  );
};

export const query = graphql`
  query homepageQuery {
    allFile {
      nodes {
        childImageSharp {
          fluid(maxWidth: 2000, quality: 90) {
            ...GatsbyImageSharpFluid_withWebp
            originalName
          }
        }
      }
    }

    sanityHome {
      hello {
        elemRight
        elemLeft
        heroText {
          text
          doRotate
          delay
          _key
        }
      }
      letsBeHonest {
        paraTwo
        paraOne
        image {
          asset {
            fluid(maxHeight: 600) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
      backToUni {
        main {
          annotation
          title
          showLine
        }
        accent {
          text
          image {
            asset {
              fluid(maxWidth: 420) {
                ...GatsbySanityImageFluid
              }
            }
          }
        }
      }
      duringUni {
        paraTwo
        paraOne
      }
    }    
  }
`;

export default IndexPage;
