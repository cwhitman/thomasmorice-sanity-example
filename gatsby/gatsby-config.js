module.exports = {
  siteMetadata: {
    title: `Thomas Morice Portfolio`,
    description: `Welcome to my website, discover more about how I became a passionate web developer and my different experiences.`,
    author: `@thomasmorice`,
    siteUrl: "https://thomasmorice.com",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /\.inline\.svg$/,
        },
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-sanity`,
      options: {
        projectId: `s01i3gvr`,
        dataset: `production`,
        watchMode: true,
      },
    },
    `gatsby-plugin-sitemap`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Thomas Morice Portfolio`,
        short_name: `Thomas Morice Portfolio`,
        start_url: `/`,
        background_color: `#2b2424`,
        theme_color: `#2b2424`,
        display: `minimal-ui`,
        icon: `src/images/thomas-favicon.png`, // This path is relative to the root of the site.
      },
    },
    "gatsby-plugin-offline",
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-174262097-1",
        // Defers execution of google analytics script after page load
        defer: true,
      },
    },
    {
      resolve: `gatsby-plugin-webfonts`,
      options: {
        fonts: {
          google: [
            {
              family: "Montserrat",
              variants: [
                "100",
                "300",
                "300italic",
                "500",
                "500italic",
                "600",
                "900",
              ],
              fontDisplay: "block",
              strategy: "selfHosted",
            },
            {
              family: "Playfair Display",
              variants: ["400", "700"],
              fontDisplay: "block",
              strategy: "selfHosted",
            },
          ],
        },
        useMinify: true,
        usePreload: true,
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
