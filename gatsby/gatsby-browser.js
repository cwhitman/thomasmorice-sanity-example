import { wrapRootElement as wrap } from "./root-wrapper";
export const onClientEntry = async () => {
  if (typeof IntersectionObserver === `undefined`) {
    await import(`intersection-observer`);
  }
};

export const wrapRootElement = wrap;
