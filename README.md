# To set up this repo for using your own Sanity project

1. Initialize a new sanity project via command line
2. Copy over the `schemas` folder in the sanity subfolder to your new project
3. Run `sanity deploy && sanity graphql deploy` to get your Sanity project live
4. Populate the project with content; I didn't add any validation or error checking to Thomas's code, so you will get errors if you don't do this.
5. In `gatsby-config` update the project ID to match your Sanity project.
6. Read up on [Sanity's Docs](https://www.sanity.io/docs) and go to town extending this!

There are lots of optimization opportunities in the Gatsby code as well.