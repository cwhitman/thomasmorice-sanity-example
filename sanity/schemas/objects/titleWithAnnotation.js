export default {
  title: 'Title with Annotation',
  name: 'titleWithAnnotation',
  type: 'object',
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string',
    },
    {
      title: 'Annotation',
      name: 'annotation',
      type: 'string',
    },
    {
      title: 'Left Line?',
      name: 'showLine',
      type: 'boolean',
    },
  ]
}