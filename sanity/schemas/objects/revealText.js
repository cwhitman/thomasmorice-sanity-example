export default {
  title: 'Reveal Text',
  name: 'revealText',
  type: 'object',
  fields: [
    {
      title: 'Text',
      name: 'text',
      type: 'string',
    },
    {
      title: 'Delay',
      name: 'delay',
      type: 'number',
    },
    {
      title: 'Rotate',
      name: 'doRotate',
      type: 'boolean',
    },
  ]
}