export default {
  title: 'Hello',
  name: 'hello',
  type: 'object',
  fieldsets: [
    {name: 'headline', title: 'Headline'},
  ],
  fields: [
    {
      title: 'Left Element',
      name: 'elemLeft',
      type: 'string',
      fieldset: 'headline' 
    },
    {
      title: 'Right Element',
      name: 'elemRight',
      type: 'string',
      fieldset: 'headline'
    },
    {
      title: 'Hero Reveal Elements',
      name: 'heroText',
      type: 'array',
      of: [
        {type: 'revealText'}
      ]
    },
  ]
}