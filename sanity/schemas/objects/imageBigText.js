export default {
  title: 'Image with Big Text',
  name: 'imageBigText',
  type: 'object',
  fields: [
    {
      title: 'Image',
      name: 'image',
      type: 'image',
    },
    {
      title: 'Text',
      name: 'text',
      type: 'string',
    },
  ]
}