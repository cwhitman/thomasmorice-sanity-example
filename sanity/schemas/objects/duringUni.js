export default {
  title: 'During University',
  name: 'duringUni',
  type: 'object',
  fields: [
    {
      title: 'Paragraph 1',
      name: 'paraOne',
      type: 'text',
    },
    {
      title: 'Paragraph 2',
      name: 'paraTwo',
      type: 'text',
    },
  ]
}