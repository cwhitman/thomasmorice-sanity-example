export default {
  title: 'Let\'s Be Honest',
  name: 'letsBeHonest',
  type: 'object',
  fields: [
    {
      title: 'Paragraph 1',
      name: 'paraOne',
      type: 'text',
    },
    {
      title: 'Paragraph 2',
      name: 'paraTwo',
      type: 'text',
    },
    {
      title: 'Image',
      name: 'image',
      type: 'image',
    },
  ]
}