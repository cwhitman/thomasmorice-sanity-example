export default {
  title: 'Back To University',
  name: 'backToUni',
  type: 'object',
  fields: [
    {
      title: 'Main Content',
      name: 'main',
      type: 'titleWithAnnotation',
    },
    {
      title: 'Section Accent',
      name: 'accent',
      type: 'imageBigText',
    },
  ]
}