// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type'

// Import document schemas
import home from './documents/home'

// Import object scheams
import revealText from './objects/revealText'
import imageBigText from './objects/imageBigText'
import titleWithAnnotation from './objects/titleWithAnnotation'

// import section object schemas
import hello from './objects/hello'
import letsBeHonest from './objects/letsBeHonest'
import backToUni from './objects/backToUni'
import duringUni from './objects/duringUni'

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'default',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
    // Pages
    home,

    // Sections
    hello,
    letsBeHonest,
    backToUni,
    duringUni,

    // Objects
    revealText,
    imageBigText,
    titleWithAnnotation
  ])
})
