export default {
  title: 'Homepage',
  name: 'home',
  type: 'document',
  fieldsets: [
  ],
  fields: [
    {
      title: 'Title',
      name: 'title',
      type: 'string',
    },
    {
      title: 'Hello',
      name: 'hello',
      type: 'hello',
      options: {
        collapsible: true,
        collapsed: true,
      }
    },
    {
      title: 'Let\'s Be Honest',
      name: 'letsBeHonest',
      type: 'letsBeHonest',
      options: {
        collapsible: true,
        collapsed: true,
      }
    },
    {
      title: 'Back to University',
      name: 'backToUni',
      type: 'backToUni',
      options: {
        collapsible: true,
        collapsed: true,
      }
    },
    {
      title: 'During University',
      name: 'duringUni',
      type: 'duringUni',
      options: {
        collapsible: true,
        collapsed: true,
      }
    },
  ]
}